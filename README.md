# Moonriver Validator Suite

Questo repo contiente il playbook Ansible per eseguire il deploy ed aggiornare un nodo validatore su network Moonriver e la suite per creare/gestire le relative chiavi SSH.

## Setup

Installiamo il progetto globalmente nella nostra macchina locale.

```bash
 npm install -g
```

Ora che abbiamo accesso al comando 'moonriver' utilizziamolo per copiare i sample di configurazione nei relativi files con la giusta estenzione.

```bash
moonriver setup-config
```

Poi andiamo a creare le cartelle che ospiteranno le nostre chiavi SSH.

```bash
moonriver setup-folders
```

A questo punto creiamo la chiave per il primo nodo validatore.
Poi andiamo a creare le cartelle che ospiteranno le nostre chiavi SSH.

```bash
moonriver setup-keys
```

## Configurazione

Sostituiamo tutti i parametri in ansible/inventory.yml necessari ad allineare il progetto alle specifiche del nostro server.

## Deploy

Carichiamo innanzi tutto la chiave SSH appena creata nel keychain di sitema così che Ansible possa utilizzarla per autenticarsi da remoto al server ed eseguire in maniera automatizzata le varie tasks.

```bash
moonriver init
```

o semplicemente

```bash
moonriver
```

Infine lanciamo ansible ed aspettiamo che ci ritorni il messaggio di conferma.

```bash
./ansible/setup.sh
```
