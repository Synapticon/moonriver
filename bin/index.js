#!/usr/bin/env node

const {
    exec
} = require("child_process")

const command = process.argv[2] || null

switch (command) {
    case null:
        init()
        break
    case 'init':
        init()
        break
    case 'start':
        start()
        break
    case 'setup-config':
        setupConfig()
        break
    case 'setup-folders':
        setupFolders()
        break
    case 'setup-keys':
        setupKeys()
        break
    default:
        console.error('Validator command not found.')
}

function setupConfig() {
    exec(`cp -n ansible/inventory.sample ansible/inventory.yml && cp -n config/remote.sample config/remote.json`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`${stdout}`)
    })
}

function setupFolders() {
    const {
        validators
    } = require(`${process.cwd()}/config/remote.json`)

    let networks = []
    let foldersCommand = ''

    validators.forEach(node => {
        if (!networks.includes(node.chain)) {
            networks.push(node.chain)
        }
    })

    networks.forEach((network, index) => {
        foldersCommand += `mkdir -p keys/${network}`

        if (index < networks.length - 1) {
            foldersCommand += ` && `
        }
    })

    exec(foldersCommand, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`${stdout}`)
    })
}

function setupKeys() {
    exec(`node bin/lib/generate-ssh-keys.js`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`${stdout}`)
    })
}

function init() {
    exec(`node bin/lib/add-ssh-keys.js`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`${stdout}`)
    })
}

function start() {
    exec(`./ansible/setup.sh`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`${stdout}`)
    })
}