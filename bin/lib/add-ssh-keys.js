const {
    exec
} = require("child_process")

const {
    validators
} = require(`${process.cwd()}/config/remote.json`)

for (let i = 0; i < validators.length; i++) {

    let chain = validators[i].chain

    exec(`ssh-add -K ${process.cwd()}/keys/${chain}/validator_${i}_rsa`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`)
            return
        }
        if (stderr) {
            console.log(`${stderr}`)
            return
        }

        console.log(`stdout: ${stdout}`)
    })
}